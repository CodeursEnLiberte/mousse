import falcon
import json
import zeep

class Stations:
    def __init__(self):
        self.get_stations = zeep.Client('wsdl').service.getStations

    def get(self, name):
        resp = self.get_stations(station={'name': name})
        return resp.stations

    def as_dict(self, station):
        return {
            'id': station.id,
            'name': station.name,
            'line': {
                'id': station.line.id,
                'number': station.line.code,
                'stif_id': station.line.codeStif,
                'name': station.line.name,
                'network': {
                    'id': station.line.reseau.code,
                    'name': station.line.reseau.name,
                }
            },
            'links': [{
                'href': 'https://mousse.codeursenliberté.fr/next_departures/{}/{}'.format(station.id, station.line.id),
                'rel': 'related',
                'title': 'Next departures on this station'
            }]
        }

    def on_get(self, req, resp):
        stations = self.get(req.get_param('q'))
        transformed = [self.as_dict(s) for s in stations]
        resp.body = json.dumps(transformed)

class NextDepartures():
    def __init__(self):
        self.get_next = zeep.Client('wsdl').service.getMissionsNext

    def as_dict(self, mission):
        return {
            'direction': mission.direction.name,
            'next_departures': mission.stationsDates,
            'messages': mission.stationsMessages,
        }

    def get(self, station_id, line_id):
        resp = self.get_next(station={'id': station_id, 'line': {'id': line_id}},
                             direction={'sens': '*'})
        return resp.missions

    def on_get(self, req, resp, station_id, line_id):
        missions = self.get(station_id, line_id)
        transformed = [self.as_dict(m) for m in missions]
        resp.body = json.dumps(transformed)

api = falcon.API()
api.add_route('/stations', Stations())
api.add_route('/next_departures/{station_id}/{line_id}', NextDepartures())
